package kimdv.ticgo.demo.entity

import javax.persistence.*

@Entity
data class SeatSelection(
        @ManyToOne
        var seatDetail: Seat? = null) {

    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToMany
    var seats = mutableListOf<ShowTimeSeat>()
}