package kimdv.ticgo.demo.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class ShowTimeSeat(
        var status: Boolean = false,
        @Column(name = "seat_showtime_row")
        var row: String? = null,
        @Column(name = "seat_showtime_column")
        var column: String? = null
){
    @Id
    @GeneratedValue
    var id:Long? = null
}

