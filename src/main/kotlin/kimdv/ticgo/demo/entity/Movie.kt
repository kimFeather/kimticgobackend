package kimdv.ticgo.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany


@Entity
data class Movie(
        var name: String? = null,
        var imageUrl: String? = null,
        var duration: Int? = null,

        var subtitle: String? = null

) {
    @Id
    @GeneratedValue
    var id: Long? = null
    var soundtrack: Array<String>? = null

}