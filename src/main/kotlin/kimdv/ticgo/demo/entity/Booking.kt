package kimdv.ticgo.demo.entity

import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToMany

@Entity
data class Booking(
        var showtimeId :Long?=null,
        var createdDateTime:Long?=null
){
    @Id
    @GeneratedValue
    var id:Long?=null
    @ManyToMany
    var seats = mutableListOf<ShowTimeSeat>()
}