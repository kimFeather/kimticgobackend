package kimdv.ticgo.demo.entity.dto

data class CinemaDto(
        var name:String?=null,
        var seats:List<SeatDto> ?=null
)