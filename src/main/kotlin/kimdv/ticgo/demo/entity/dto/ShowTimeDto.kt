package kimdv.ticgo.demo.entity.dto

data class ShowTimeDto(
        var startDateTime:Long?=null,
        var endDateTime:Long?=null,
        var movie: MovieDto?=null,
        var cinema: CinemaDto?=null,
        var seats : List<SeatSelectionDto>?=null
)