package kimdv.ticgo.demo.entity.dto

data class MovieDto (
        var name: String? = null,
        var imageUrl: String? = null,
        var duration: Int? = null,
        var soundtrack: Array<String>? = null,
        var subtitle: String? = null,
        var id: Long? = null
)