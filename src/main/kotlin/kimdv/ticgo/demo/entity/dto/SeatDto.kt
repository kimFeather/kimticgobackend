package kimdv.ticgo.demo.entity.dto

import kimdv.ticgo.demo.entity.SeatType

class SeatDto (
        var name:String?=null,
        var seatType: SeatType?=null,
        var price:Double?=null,
        var rows:Int?=null,
        var columns:Int?=null
)