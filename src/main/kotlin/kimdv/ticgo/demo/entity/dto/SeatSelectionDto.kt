package kimdv.ticgo.demo.entity.dto

import kimdv.ticgo.demo.entity.Seat


data class SeatSelectionDto(
        var seatDetail: Seat?= null,
        var seats: List<ShowTimeSeatDto>? = null
)

data class ShowTimeSeatDto(
        var status : Boolean = false,
        var row: String? = null ,
        var column: String? = null

)