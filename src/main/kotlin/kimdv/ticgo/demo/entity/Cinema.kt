package kimdv.ticgo.demo.entity

import javax.persistence.*

@Entity
data class Cinema (
        var name:String

){
    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToMany
    var seat = mutableListOf<Seat>()
}