package kimdv.ticgo.demo.entity

import java.time.LocalDate
import java.util.*
import javax.persistence.*

@Entity
data class ShowTime(
        var startDateTime: Long? = null,
        var endDateTime: Long? = null
) {

    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToOne
    var movie: Movie? = null

    @OneToOne
    var cinema: Cinema? =null

    @OneToMany
    var seatsSelection = mutableListOf<SeatSelection>()



    constructor(
            movie: Movie,
            startDateTime: Long,
            endDateTime: Long
    ) :
            this(startDateTime, endDateTime) {
        this.movie = movie
    }

}





