package kimdv.ticgo.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class UserData(
        override var email:String?=null,
        override var password:String?=null,
        var firstName:String?=null,
        var lastName:String?=null,
        var image:String?=null
):User {
    @Id
    @GeneratedValue
    var id:Long?=null
}

data class UserInfoDto(
        var firstName:String?=null,
        var lastName:String?=null,
        var id:Long?=null
)
