package kimdv.ticgo.demo.controller

import kimdv.ticgo.demo.entity.Booking
import kimdv.ticgo.demo.service.BookingService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class BookingController{
    @Autowired
    lateinit var bookingService: BookingService

    @PostMapping("/booking")
    fun postBooking(@RequestBody booking: Booking): ResponseEntity<Any> {
        val bookings = bookingService.save(booking)
        return ResponseEntity.ok(bookings)
    }

}