package kimdv.ticgo.demo.controller

import kimdv.ticgo.demo.service.MovieService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class MovieController {

    @Autowired
    lateinit var movieService: MovieService

    @GetMapping("/movies")
    fun getProducts(): ResponseEntity<Any> {
        val movies = movieService.getMovies()
        return ResponseEntity.ok(movies)
    }

}
