package kimdv.ticgo.demo.controller

import kimdv.ticgo.demo.entity.UserInfoDto
import kimdv.ticgo.demo.entity.dto.UserDataDto
import kimdv.ticgo.demo.entity.dto.UserRegisterDto
import kimdv.ticgo.demo.service.UserDataService
import kimdv.ticgo.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class UserDataController{
    @Autowired
    lateinit var userDataService: UserDataService

    @PostMapping("/users/register")
    fun register(@RequestBody userRegisterDto: UserRegisterDto): ResponseEntity<Any> {
        val output = userDataService.save(MapperUtil.INSTANT.mapUserRegisterDto(userRegisterDto))
        return ResponseEntity.ok(output)
    }

    @PutMapping("/user/edit/data")
    fun editUser(@RequestBody userInfoDto: UserInfoDto):ResponseEntity<Any>{
        val output= MapperUtil.INSTANT.mapUserInfoDto(userDataService.saveInfo(userInfoDto))
        return ResponseEntity.ok(output)

    }
//    @GetMapping("/users")
//    fun getAllUsers(@RequestBody userDataDto: UserDataDto) : ResponseEntity<Any>{
//        val users= UserService.getAllCinema()
//        return ResponseEntity.ok(MapperUtil.INSTANT.mapCinemaDto(cinemas))
//    }

}