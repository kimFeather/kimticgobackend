package kimdv.ticgo.demo.controller

import kimdv.ticgo.demo.service.ShowTimeService
import kimdv.ticgo.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ShowTimeController{
    @Autowired
    lateinit var showtimeService: ShowTimeService

    @GetMapping("/showtimes")
    fun getAllShowtimes(): ResponseEntity<Any> {
        val showtimes = showtimeService.getAllShowTimes()
        return ResponseEntity.ok(MapperUtil.INSTANT.mapShowtimeDto(showtimes))
    }

}