package kimdv.ticgo.demo.controller

import kimdv.ticgo.demo.service.CinemaService
import kimdv.ticgo.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class CinemaController{
    @Autowired
    lateinit var cinemaService: CinemaService

    @GetMapping("/cinema")
    fun getAllCinema(): ResponseEntity<Any> {
        val cinemas= cinemaService.getAllCinema()
        return ResponseEntity.ok(MapperUtil.INSTANT.mapCinemaDto(cinemas))
    }

}