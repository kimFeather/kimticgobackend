package kimdv.ticgo.demo.util

import kimdv.ticgo.demo.entity.*
import kimdv.ticgo.demo.entity.dto.*
import org.mapstruct.InheritConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil{
    companion object {
        val INSTANT = Mappers.getMapper(MapperUtil::class.java)
    }

    @Mappings(
            Mapping(source = "seat", target = "seats")
    )
    fun mapCinemaDto(cinema: Cinema): CinemaDto
    fun mapCinemaDto(cinemas: List<Cinema>):List<CinemaDto>


    fun mapSeatDto(seat: Seat): SeatDto
    fun mapSeatDto(seat: List<Seat>):List<SeatDto>

    fun mapMovieDto(movie: Movie): MovieDto
    fun mapMovieDto(movie: List<Movie>):List<MovieDto>
//    @Mappings(
//            Mapping(source = "selectedSeat", target = "seats")

//    )
    fun mapShowtimeDto(showtime: ShowTime): ShowTimeDto
    fun mapShowtimeDto(showtime: List<ShowTime>):List<ShowTimeDto>

    fun mapSelectSeatDto(seatSelection: SeatSelection):SeatSelectionDto
    fun mapSelectSeatDto(seatSelection: List<SeatSelection>):List<SeatSelectionDto>



    fun mapUserDataDto(userData: UserData):UserDataDto
    fun mapUserDataDto(userData: List<UserData>):List<UserDataDto>

    fun mapUserInfoDto(userData: UserData):UserInfoDto

    @InheritConfiguration
    fun mapUserInfoDto(userInfoDto: UserInfoDto):UserData

    @InheritConfiguration
    fun mapUserRegisterDto(userRegisterDto: UserRegisterDto):UserData




}
