package kimdv.ticgo.demo.config

import kimdv.ticgo.demo.entity.*
import kimdv.ticgo.demo.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import javax.transaction.Transactional
import java.sql.Timestamp

@Component
class ApplicationLoader: ApplicationRunner {

    @Autowired
    lateinit var movieRepository: MovieRepository

    @Autowired
    lateinit var seatRepository: SeatRepository

    @Autowired
    lateinit var cinemaRepository: CinemaRepository

    @Autowired
    lateinit var seatSelectionRepository:SeatSelectionRepository

    @Autowired
    lateinit var showtimeRepository: ShowTimeReporitory

    @Autowired
    lateinit var showtimeSeatRepository: ShowTimeSeatRepository

    @Transactional
    override fun run(args: ApplicationArguments?) {
        var movie1 = movieRepository.save(Movie("Captain Marvel",
                "https://lh3.googleusercontent.com/3Ost-FvXYMVAs84hLxJAn9qqIh9s_YCC8oCySCuOWIJWIu8zPIuKYJ9FG_FEFBqKZoQnguVOuNAkg9kArkPM=w260",
                220,
                "TH"
        ))
            movie1.soundtrack = arrayOf("EN,TH")

        var movie2 = movieRepository.save(Movie("SHAZAM!",
                "https://lh3.googleusercontent.com/gaToaVcS_L0LvoVdxAvdNvlm1u1-nKd03k0FhiY1t2pqzNBq0LjCEdXw8_Bumu8v7S7gdCtaf5awZUseZJ2K=w260",
                210,
                "TH"

        ))
        movie2.soundtrack = arrayOf("EN,TH")

        var movie3 = movieRepository.save(Movie("Cadaver",
                "https://lh3.googleusercontent.com/eIyJuFvni5AqQMeQhC5rvCEgJa6GcNEZtzFBI0Hv3zUxTm_RM-ZgLaasc5yvXjctAeRtDp7OdkOZBQy_u5s=w260",
                200,
                "TH"

        ))
        movie3.soundtrack = arrayOf("EN,TH")

        var movie4 = movieRepository.save(Movie("ซิตี้ฮันเตอร์ สายลับ คาสโนเวอร์",
                "https://lh3.googleusercontent.com/eZsJHzkpggsX7nu0oNCsq-BKdbR3fMQAIGnbedw1duD54g0D1mJ3MZtHWxB6CrNzgtLBe7rpHxC0P0nZrD6M=w260",
                190,
                "TH"

        ))
        movie4.soundtrack = arrayOf("EN,TH")

        var movie5 = movieRepository.save(Movie("Green Book",
                "https://lh3.googleusercontent.com/_sO7q8QPGtO_9zNxW-KkTKtfwgGWPm2rRyW3UcG7_KIlAszFGZ93WoM00YD-Y6PP3QL7Wh6dJpJKyK2qs25exg=w260",
                180,
                "TH"

        ))
        movie5.soundtrack = arrayOf("EN,TH")
        var movie6 = movieRepository.save(Movie("Dumbo",
                "https://lh3.googleusercontent.com/FcKcdorsSc0A1gcPJ6Vgwjd1oVU0wgZP6m6rvOEj-INliNOeILq-ND3pR41N2RZCCP5scslco2shHqaVIq7c=w260",
                170,
                "TH"

        ))
        movie6.soundtrack = arrayOf("EN,TH")

//        var rowA = seatRepository.save(Seat(SeatType.PREMIUM,"150","1",""))

        var deluxe= seatRepository.save(Seat("Deluxe",SeatType.DELUXE,200,10,20))
        var premium= seatRepository.save(Seat("Premium", SeatType.PREMIUM,220,4,20))
        var sofa= seatRepository.save(Seat("Duo Sofa",SeatType.SOFA,700,1,6))

        var cinema1 = cinemaRepository.save(Cinema("Cinema1"))
        var cinema2 = cinemaRepository.save(Cinema("Cinema2"))
        var cinema3 = cinemaRepository.save(Cinema("Cinema3"))

        cinema1.seat = mutableListOf(sofa,premium,deluxe)
        cinema2.seat = mutableListOf(premium,deluxe)
        cinema3.seat = mutableListOf(sofa,premium,deluxe)


        var movieShowTime = showtimeRepository.save(ShowTime(Timestamp(1553396400000).time, Timestamp(1553393100000).time))

        movieShowTime.movie = movie1
        movieShowTime.cinema = cinema1

        var row:Char = 'A'
        for ((index, item) in movieShowTime.cinema!!.seat.withIndex()) {
            var seatSelection = seatSelectionRepository.save(SeatSelection(item))
            for (indexR in 1..seatSelection.seatDetail!!.rows!!) {
                for (indexC in 1..seatSelection.seatDetail!!.columns!!) {
                    var seat: ShowTimeSeat
                    if (item.seatType == SeatType.SOFA){
                        seat = showtimeSeatRepository.save(ShowTimeSeat(false,"${row}${row}","${indexC}"))
                    }else{
                        seat = showtimeSeatRepository.save(ShowTimeSeat(false, "${row}", "${indexC}"))
                    }
                    seatSelection.seats.add(seat)
                }
                row++
            }
            movieShowTime.seatsSelection.add(seatSelection)
            if(item.seatType === SeatType.SOFA){
                row = 'A'
            }
        }

//        var row:Char = 'A'
//        for ((index, item) in movieShowTime.cinema!!.seat.withIndex()) {
//            var seatSelection = seatSelectionRepository.save(SeatSelection(item))
//            for (indexR in 1..seatSelection.seatDetail!!.rows!!) {
//                for (indexC in 1..seatSelection.seatDetail!!.columns!!) {
//                    var seat = showtimeSeatRepository.save(ShowTimeSeat(false, "${row}", "${indexC}"))
//                    seatSelection.seats.add(seat)
//                }
//                row++
//            }
//            movieShowTime.seatsSelection.add(seatSelection)
//        }





    }
}