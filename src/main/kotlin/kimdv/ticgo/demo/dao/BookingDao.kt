package kimdv.ticgo.demo.dao

import kimdv.ticgo.demo.entity.Booking

interface BookingDao{
    fun save(book: Booking):Booking

}