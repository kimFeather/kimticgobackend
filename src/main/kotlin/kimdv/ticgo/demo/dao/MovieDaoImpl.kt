package kimdv.ticgo.demo.dao

import kimdv.ticgo.demo.entity.Movie
import kimdv.ticgo.demo.repository.MovieRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class MovieDaoImpl : MovieDao{

    @Autowired
    lateinit var movieRepository: MovieRepository

    override fun getMovies(): List<Movie> {
        return movieRepository.findAll().filterIsInstance(Movie::class.java)
    }
}
