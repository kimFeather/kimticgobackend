package kimdv.ticgo.demo.dao

import kimdv.ticgo.demo.entity.ShowTime
import kimdv.ticgo.demo.repository.ShowTimeReporitory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ShowTimeDaoImpl:ShowTimeDao{
    override fun findById(id: Long?): ShowTime {
        return showtimeRepository.findById(id!!).orElse(null)
    }

    override fun getAllShowtimes(): List<ShowTime> {
        return showtimeRepository.findAll().filterIsInstance(ShowTime::class.java)
    }

    @Autowired
    lateinit var showtimeRepository: ShowTimeReporitory
}