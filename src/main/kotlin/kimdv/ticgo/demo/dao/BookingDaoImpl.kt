package kimdv.ticgo.demo.dao

import kimdv.ticgo.demo.entity.Booking
import kimdv.ticgo.demo.repository.BookingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class BookingDaoImpl:BookingDao{
    override fun save(book: Booking): Booking {
        return bookingRepository.save(book)
    }

    @Autowired
    lateinit var bookingRepository: BookingRepository
}