package kimdv.ticgo.demo.dao

import kimdv.ticgo.demo.entity.ShowTimeSeat
import kimdv.ticgo.demo.repository.ShowTimeSeatRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository


@Profile("db")
@Repository
class ShowTimeSeatDaoImpl : ShowTimeSeatDao {
    override fun findById(id: Long?): ShowTimeSeat {
        return seatInShowtimeRepository.findById(id!!).orElse(null)
    }

    override fun save(seat: ShowTimeSeat): ShowTimeSeat {
        return seatInShowtimeRepository.save(seat)
    }

    @Autowired
    lateinit var seatInShowtimeRepository: ShowTimeSeatRepository

}