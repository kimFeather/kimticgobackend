package kimdv.ticgo.demo.dao

import kimdv.ticgo.demo.entity.Seat
import kimdv.ticgo.demo.entity.SeatType
import kimdv.ticgo.demo.repository.SeatRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class SeatDaoImpl:SeatDao{
    @Autowired
    lateinit var seatRepository: SeatRepository
    override fun getSeats(): List<Seat> {
        return seatRepository.findAll().filterIsInstance(Seat::class.java)
    }

}