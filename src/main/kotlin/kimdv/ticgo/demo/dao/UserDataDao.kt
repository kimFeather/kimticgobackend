package kimdv.ticgo.demo.dao

import kimdv.ticgo.demo.entity.UserData

interface UserDataDao {
    fun save(userRegister: UserData): UserData
    fun findById(id: Long?): UserData
}