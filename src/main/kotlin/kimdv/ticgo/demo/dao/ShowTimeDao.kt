package kimdv.ticgo.demo.dao

import kimdv.ticgo.demo.entity.ShowTime

interface ShowTimeDao{
    fun getAllShowtimes(): List<ShowTime>
    abstract fun findById(id: Long?): ShowTime
}