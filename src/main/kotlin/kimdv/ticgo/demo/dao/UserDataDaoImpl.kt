package kimdv.ticgo.demo.dao

import kimdv.ticgo.demo.entity.UserData
import kimdv.ticgo.demo.repository.UserDataRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class UserDataDaoImpl:UserDataDao{
    override fun findById(id: Long?): UserData {
        return userDataRepository.findById(id!!).orElse(null)
    }

    override fun save(userRegister: UserData): UserData {
        return userDataRepository.save(userRegister)
    }

    @Autowired
    lateinit var userDataRepository: UserDataRepository
}