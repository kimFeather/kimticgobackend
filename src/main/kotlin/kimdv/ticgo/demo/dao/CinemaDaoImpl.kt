package kimdv.ticgo.demo.dao

import kimdv.ticgo.demo.entity.Cinema
import kimdv.ticgo.demo.repository.CinemaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CinemaDaoImpl:CinemaDao{
    override fun getAllCinema(): List<Cinema> {
        return cinemaRepository.findAll().filterIsInstance(Cinema::class.java)
    }
    @Autowired
    lateinit var cinemaRepository: CinemaRepository
}