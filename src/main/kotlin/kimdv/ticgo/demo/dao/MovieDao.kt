package kimdv.ticgo.demo.dao

import kimdv.ticgo.demo.entity.Movie

interface MovieDao {
    fun getMovies(): List<Movie>
}
