package kimdv.ticgo.demo.dao

import kimdv.ticgo.demo.entity.Cinema

interface CinemaDao{
    fun getAllCinema(): List<Cinema>
}