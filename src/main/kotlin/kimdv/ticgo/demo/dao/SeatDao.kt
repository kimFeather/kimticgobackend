package kimdv.ticgo.demo.dao

import kimdv.ticgo.demo.entity.Seat

interface SeatDao{
    fun getSeats():List<Seat>
}