package kimdv.ticgo.demo.dao

import kimdv.ticgo.demo.entity.ShowTimeSeat

interface ShowTimeSeatDao {
    fun findById(id: Long?): ShowTimeSeat
    fun save(seat: ShowTimeSeat): ShowTimeSeat

}