package kimdv.ticgo.demo.service

import kimdv.ticgo.demo.dao.MovieDao
import kimdv.ticgo.demo.entity.Movie
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MovieServiceImpl : MovieService {

    @Autowired
    lateinit var movieDao: MovieDao


    override fun getMovies(): List<Movie> {
        return movieDao.getMovies()
    }

}
