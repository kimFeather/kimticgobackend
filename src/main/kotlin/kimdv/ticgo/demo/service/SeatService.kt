package kimdv.ticgo.demo.service

import kimdv.ticgo.demo.entity.Seat

interface SeatService{
    fun getSeats():List<Seat>
}