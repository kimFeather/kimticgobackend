package kimdv.ticgo.demo.service

import kimdv.ticgo.demo.entity.UserData
import kimdv.ticgo.demo.entity.UserInfoDto

interface UserDataService{

    fun save(userRegister: UserData): UserData
    fun saveInfo(userInfoDto: UserInfoDto): UserData
    //    abstract fun saveInfo(userInfoDto: UserInfoDto): UserData
//    abstract fun save(userRegister: UserData): UserData


}