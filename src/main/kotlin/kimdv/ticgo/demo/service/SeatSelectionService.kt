package kimdv.ticgo.demo.service

import kimdv.ticgo.demo.entity.SeatSelection

interface SeatSelectionService{
    fun getAllSeatSelection(): List<SeatSelection>

}