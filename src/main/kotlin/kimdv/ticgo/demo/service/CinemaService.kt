package kimdv.ticgo.demo.service

import kimdv.ticgo.demo.entity.Cinema

interface CinemaService{
    fun getAllCinema(): List<Cinema>

}