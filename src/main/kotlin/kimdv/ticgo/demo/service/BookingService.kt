package kimdv.ticgo.demo.service

import kimdv.ticgo.demo.entity.Booking

interface BookingService{
    fun save(booking: Booking): Booking

}