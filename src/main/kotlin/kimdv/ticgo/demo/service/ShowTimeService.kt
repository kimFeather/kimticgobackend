package kimdv.ticgo.demo.service

import kimdv.ticgo.demo.entity.ShowTime

interface ShowTimeService{
    fun getAllShowTimes():List<ShowTime>
}