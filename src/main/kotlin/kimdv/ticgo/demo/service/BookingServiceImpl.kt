package kimdv.ticgo.demo.service

import kimdv.ticgo.demo.dao.BookingDao
import kimdv.ticgo.demo.dao.ShowTimeDao
import kimdv.ticgo.demo.dao.ShowTimeSeatDao
import kimdv.ticgo.demo.entity.Booking
import kimdv.ticgo.demo.entity.ShowTimeSeat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BookingServiceImpl:BookingService{
    override fun save(booking: Booking): Booking {
        var showtimeId = showtimeDao.findById(booking.showtimeId)
        var seats = mutableListOf<ShowTimeSeat>()
        for( item in booking.seats) {
            var seat = showTimeSeatDao.findById(item.id)
            seat.status = true
            seats.add(showTimeSeatDao.save(seat))
        }
        var book = Booking()
        book.showtimeId = showtimeId.id
        book.seats = seats
        book.createdDateTime = System.currentTimeMillis()
        bookingDao.save(book)
        return book
    }

    @Autowired
    lateinit var bookingDao: BookingDao
    @Autowired
    lateinit var showtimeDao: ShowTimeDao
    @Autowired
    lateinit var showTimeSeatDao: ShowTimeSeatDao
}