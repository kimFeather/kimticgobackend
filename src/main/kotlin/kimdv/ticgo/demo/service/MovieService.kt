package kimdv.ticgo.demo.service

import kimdv.ticgo.demo.entity.Movie


interface MovieService {
    fun getMovies(): List<Movie>
}
