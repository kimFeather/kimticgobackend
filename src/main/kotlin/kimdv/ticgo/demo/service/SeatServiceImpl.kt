package kimdv.ticgo.demo.service

import kimdv.ticgo.demo.dao.SeatDao
import kimdv.ticgo.demo.entity.Seat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class SeatServiceImpl:SeatService{
    override fun getSeats(): List<Seat> {
        return seatDao.getSeats()
    }

    @Autowired
    lateinit var seatDao: SeatDao
}