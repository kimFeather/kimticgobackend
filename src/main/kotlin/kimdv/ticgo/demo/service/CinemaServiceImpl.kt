package kimdv.ticgo.demo.service

import kimdv.ticgo.demo.dao.CinemaDao
import kimdv.ticgo.demo.entity.Cinema
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CinemaServiceImpl:CinemaService{
    override fun getAllCinema(): List<Cinema> {
        return cinemaDao.getAllCinema()
    }
    @Autowired
    lateinit var cinemaDao: CinemaDao
}