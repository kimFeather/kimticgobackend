package kimdv.ticgo.demo.service

import kimdv.ticgo.demo.dao.UserDataDao
import kimdv.ticgo.demo.entity.UserData
import kimdv.ticgo.demo.entity.UserInfoDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class UserDataServiceImpl:UserDataService{
    override fun saveInfo(userInfoDto: UserInfoDto): UserData {
        val user = userDataDao.findById(userInfoDto.id)
        user.firstName = userInfoDto.firstName
        user.lastName = userInfoDto.lastName
        return userDataDao.save(user)
    }


    override fun save(userRegister: UserData): UserData {
        return userDataDao.save(userRegister)
    }

    @Autowired
    lateinit var userDataDao:UserDataDao
}