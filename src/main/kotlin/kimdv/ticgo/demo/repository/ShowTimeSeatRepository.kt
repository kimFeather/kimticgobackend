package kimdv.ticgo.demo.repository

import kimdv.ticgo.demo.entity.ShowTimeSeat
import org.springframework.data.repository.CrudRepository

interface ShowTimeSeatRepository : CrudRepository<ShowTimeSeat, Long>