package kimdv.ticgo.demo.repository

import kimdv.ticgo.demo.entity.ShowTime
import org.springframework.data.repository.CrudRepository


interface ShowTimeReporitory : CrudRepository<ShowTime, Long>