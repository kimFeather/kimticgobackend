package kimdv.ticgo.demo.repository

import kimdv.ticgo.demo.entity.Seat
import org.springframework.data.repository.CrudRepository

interface SeatRepository: CrudRepository<Seat, Long>