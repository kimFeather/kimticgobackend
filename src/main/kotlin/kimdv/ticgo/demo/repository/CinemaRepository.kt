package kimdv.ticgo.demo.repository

import kimdv.ticgo.demo.entity.Cinema
import org.springframework.data.repository.CrudRepository

interface CinemaRepository: CrudRepository<Cinema, Long>