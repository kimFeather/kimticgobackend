package kimdv.ticgo.demo.repository

import kimdv.ticgo.demo.entity.Booking
import org.springframework.data.repository.CrudRepository


interface BookingRepository: CrudRepository<Booking, Long>