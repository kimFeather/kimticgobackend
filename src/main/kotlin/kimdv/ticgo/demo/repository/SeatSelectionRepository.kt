package kimdv.ticgo.demo.repository

import kimdv.ticgo.demo.entity.SeatSelection
import org.springframework.data.repository.CrudRepository

interface SeatSelectionRepository: CrudRepository<SeatSelection, Long>