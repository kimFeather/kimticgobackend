package kimdv.ticgo.demo.repository

import kimdv.ticgo.demo.entity.UserData
import org.springframework.data.repository.CrudRepository

interface UserDataRepository: CrudRepository<UserData, Long>