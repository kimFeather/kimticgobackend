package kimdv.ticgo.demo.repository

import kimdv.ticgo.demo.entity.Movie
import org.springframework.data.repository.CrudRepository

interface MovieRepository: CrudRepository<Movie, Long> {
}
